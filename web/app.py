from flask import Flask, render_template, abort
import os

app = Flask(__name__)

@app.route("/<name>")
def hello(name):
    valid = {".css", ".html"}
    invalid = {"~", "..", "//"}
    if invalid in name:
        abort(403)

    if os.path.isfile("templates/" + name):
        return render_template(name)
    else:
        abort(404)

@app.errorhandler(403)
def error_403(error):
    return render_template('403.html'), 403

@app.errorhandler(404)
def error_404(error):
    return render_template('404.html'), 404

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
